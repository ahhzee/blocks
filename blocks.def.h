//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{" CPU Temp: ", "sb-cputemp",			4,		99},
	{"RSS: ", "sb-rss",						0,		4},
	{"", "sb-vpn",                          0,      3},
	{"Mem: ", "sb-memory",					16,		2},
	{"", "sb-timedate",						8,		1},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;

